
<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta name="generator" content="Docutils 0.19: https://docutils.sourceforge.io/" />

    <title>Troubleshooting &#8212; Knot DNS 3.4.0 documentation</title>
    <link rel="stylesheet" type="text/css" href="_static/pygments.css" />
    <link rel="stylesheet" type="text/css" href="_static/main.css" />
    <link rel="stylesheet" type="text/css" href="_static/panels-main.c949a650a448cc0ae9fd3441c0e17fb0.css" />
    <link rel="stylesheet" type="text/css" href="_static/panels-bootstrap.5fd3999ee7762ccc51105388f4a9d115.css" />
    <link rel="stylesheet" type="text/css" href="_static/panels-variables.06eb56fa6e07937060861dad626602ad.css" />
    <script data-url_root="./" id="documentation_options" src="_static/documentation_options.js"></script>
    <script src="_static/jquery.js"></script>
    <script src="_static/underscore.js"></script>
    <script src="_static/_sphinx_javascript_frameworks_compat.js"></script>
    <script src="_static/doctools.js"></script>
    <script src="_static/sphinx_highlight.js"></script>
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="Configuration Reference" href="reference.html" />
    <link rel="prev" title="Operation" href="operation.html" /> 
  </head><body>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="reference.html" title="Configuration Reference"
             accesskey="N">next</a></li>
        <li class="right" >
          <a href="operation.html" title="Operation"
             accesskey="P">previous</a> |</li>
        <li class="nav-item nav-item-0"><a href="index.html">Knot DNS 3.4.0 documentation</a> &#187;</li>
        <li class="nav-item nav-item-this"><a href="">Troubleshooting</a></li> 
      </ul>
    </div>  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <section id="troubleshooting">
<span id="id1"></span><h1>Troubleshooting<a class="headerlink" href="#troubleshooting" title="Permalink to this heading">¶</a></h1>
<p>First of all, check the logs. Enabling at least the <code class="docutils literal notranslate"><span class="pre">warning</span></code> message
severity may help you to identify some problems. See the <a class="reference internal" href="reference.html#log-section"><span class="std std-ref">log section</span></a>
for details.</p>
<section id="reporting-bugs">
<span id="submitting-a-bugreport"></span><h2>Reporting bugs<a class="headerlink" href="#reporting-bugs" title="Permalink to this heading">¶</a></h2>
<p>If you are unable to solve the problem by yourself, you can submit a
bugreport to the Knot DNS developers. For security or sensitive issues
contact the developers directly on
<a class="reference external" href="mailto:knot-dns&#37;&#52;&#48;labs&#46;nic&#46;cz">knot-dns<span>&#64;</span>labs<span>&#46;</span>nic<span>&#46;</span>cz</a>.
All other bugs and questions may be directed to the public Knot DNS users
mailing list
(<a class="reference external" href="mailto:knot-dns-users&#37;&#52;&#48;lists&#46;nic&#46;cz">knot-dns-users<span>&#64;</span>lists<span>&#46;</span>nic<span>&#46;</span>cz</a>) or
may be entered into the
<a class="reference external" href="https://gitlab.nic.cz/knot/knot-dns/issues">issue tracking system</a>.</p>
<p>Before anything else, please try to answer the following questions:</p>
<ul class="simple">
<li><p>Has it been working?</p></li>
<li><p>What has changed? System configuration, software updates, network
configuration, firewall rules modification, hardware replacement, etc.</p></li>
</ul>
<p>The bugreport should contain the answers for the previous questions and in
addition at least the following information:</p>
<ul class="simple">
<li><p>Knot DNS version and type of installation (distribution package, from source,
etc.)</p></li>
<li><p>Operating system, platform, kernel version</p></li>
<li><p>Relevant basic hardware information (processor, amount of memory, available
network devices, etc.)</p></li>
<li><p>Description of the bug</p></li>
<li><p>Log output with the highest verbosity (category <code class="docutils literal notranslate"><span class="pre">any</span></code>, severity <code class="docutils literal notranslate"><span class="pre">debug</span></code>)</p></li>
<li><p>Steps to reproduce the bug (if known)</p></li>
<li><p>Backtrace (if the bug caused a crash or a hang; see the next section)</p></li>
</ul>
<p>If possible, please provide a minimal configuration file and zone files which
can be used to reproduce the bug.</p>
</section>
<section id="generating-backtrace">
<span id="id2"></span><h2>Generating backtrace<a class="headerlink" href="#generating-backtrace" title="Permalink to this heading">¶</a></h2>
<p>Backtrace carries basic information about the state of the program and how
the program got where it is. It helps determining the location of the bug in
the source code.</p>
<p>If you run Knot DNS from distribution packages, make sure the debugging
symbols for the package are installed. The symbols are usually distributed
in a separate package.</p>
<p>There are several ways to get the backtrace. One possible way is to extract
the backtrace from a core dump file. Core dump is a memory snapshot generated
by the operating system when a process crashes. The generating of core dumps
must be usually enabled:</p>
<div class="highlight-none notranslate"><div class="highlight"><pre><span></span>$ ulimit -c unlimited                  # Enable unlimited core dump size
$ knotd ...                            # Reproduce the crash
...
$ gdb knotd &lt;core-dump-file&gt;           # Start gdb on the core dump
(gdb) info threads                     # Get a summary of all threads
(gdb) thread apply all bt full         # Extract backtrace from all threads
(gdb) quit
</pre></div>
</div>
<p>To save the backtrace into a file, the following GDB commands can be used:</p>
<div class="highlight-none notranslate"><div class="highlight"><pre><span></span>(gdb) set pagination off
(gdb) set logging file backtrace.txt
(gdb) set logging on
(gdb) info threads
(gdb) thread apply all bt full
(gdb) set logging off
</pre></div>
</div>
<p>To generate a core dump of a running process, the <cite>gcore</cite> utility can be used:</p>
<div class="highlight-none notranslate"><div class="highlight"><pre><span></span>$ gcore -o &lt;output-file&gt; $(pidof knotd)
</pre></div>
</div>
<p>Please note that core dumps can be intercepted by an error-collecting system
service (systemd-coredump, ABRT, Apport, etc.). If you are using such a service,
consult its documentation about core dump retrieval.</p>
<p>If the error is reproducible, it is also possible to start and inspect the
server directly in the debugger:</p>
<div class="highlight-none notranslate"><div class="highlight"><pre><span></span>$ gdb --args knotd -c /etc/knot.conf
(gdb) run
...
</pre></div>
</div>
<p>Alternatively, the debugger can be attached to a running server
process. This is generally useful when troubleshooting a stuck process:</p>
<div class="highlight-none notranslate"><div class="highlight"><pre><span></span>$ knotd ...
$ gdb --pid $(pidof knotd)
(gdb) continue
...
</pre></div>
</div>
<p>If you fail to get a backtrace of a running process using the previous method,
you may try the single-purpose <code class="docutils literal notranslate"><span class="pre">pstack</span></code> utility:</p>
<div class="highlight-none notranslate"><div class="highlight"><pre><span></span>$ pstack $(pidof knotd) &gt; backtrace.txt
</pre></div>
</div>
</section>
<section id="crash-caused-by-a-bus-error">
<span id="bus-error"></span><h2>Crash caused by a Bus error<a class="headerlink" href="#crash-caused-by-a-bus-error" title="Permalink to this heading">¶</a></h2>
<p>Zone files and a configuration file are usually accessed as
<a class="reference external" href="https://pubs.opengroup.org/onlinepubs/9699919799/functions/mmap.html">mmaped</a>
files. If such files are changed or truncated at the same time when those files
are being loaded/reloaded by the program, it may result in <a class="reference external" href="https://pubs.opengroup.org/onlinepubs/9699919799/basedefs/signal.h.html">Bus error
(SIGBUS)</a>
and a program crash. If you encounter a Bus error, first check that there isn't
a concurrent write access from an external program to the respective files.</p>
</section>
</section>


            <div class="clearer"></div>
          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
            <p class="logo"><a href="index.html">
              <img class="logo" src="_static/logo.svg" alt="Logo"/>
            </a></p>
  <div>
    <h3><a href="index.html">Table of Contents</a></h3>
    <ul>
<li><a class="reference internal" href="#">Troubleshooting</a><ul>
<li><a class="reference internal" href="#reporting-bugs">Reporting bugs</a></li>
<li><a class="reference internal" href="#generating-backtrace">Generating backtrace</a></li>
<li><a class="reference internal" href="#crash-caused-by-a-bus-error">Crash caused by a Bus error</a></li>
</ul>
</li>
</ul>

  </div>
  <div>
    <h4>Previous topic</h4>
    <p class="topless"><a href="operation.html"
                          title="previous chapter">Operation</a></p>
  </div>
  <div>
    <h4>Next topic</h4>
    <p class="topless"><a href="reference.html"
                          title="next chapter">Configuration Reference</a></p>
  </div>
  <div role="note" aria-label="source link">
    <h3>This Page</h3>
    <ul class="this-page-menu">
      <li><a href="_sources/troubleshooting.rst.txt"
            rel="nofollow">Show Source</a></li>
    </ul>
   </div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/>
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>document.getElementById('searchbox').style.display = "block"</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="reference.html" title="Configuration Reference"
             >next</a></li>
        <li class="right" >
          <a href="operation.html" title="Operation"
             >previous</a> |</li>
        <li class="nav-item nav-item-0"><a href="index.html">Knot DNS 3.4.0 documentation</a> &#187;</li>
        <li class="nav-item nav-item-this"><a href="">Troubleshooting</a></li> 
      </ul>
    </div>
    <div class="footer" role="contentinfo">
        &#169; Copyright Copyright 2010–2024, CZ.NIC, z.s.p.o..
      Created using <a href="https://www.sphinx-doc.org/">Sphinx</a> 5.3.0.
    </div>
  </body>
</html>