Version 2.6.1
#############

:date: 2017-11-02
:lang: en

Features:
---------
 - NSEC3 Opt-Out support in the DNSSEC signing
 - New CDS/CDNSKEY publish configuration option

Improvements:
-------------
 - Simplified DNSSEC log message with DNSKEY details
 - +tls-hostname in kdig implies +tls-ca if neither +tls-ca nor +tls-pin is given
 - New documentation sections for DNSSEC key rollovers and shared keys
 - Keymgr no longer prints useless algorithm number for generated key
 - Kdig prints unknown RCODE in a numeric format
 - Better support for LLVM libFuzzer

Bugfixes:
---------
 - Faulty DNAME semantic check if present in the zone apex and NSEC3 is used
 - Immediate zone flush not scheduled during the zone load event
 - Server crashes upon dynamic zone addition if a query module is loaded
 - Kdig fails to connect over TLS due to SNI is set to server IP address
 - Possible out-of-bounds memory access at the end of the input
 - TCP Fast Open enabled by default in kdig breaks TLS connection
