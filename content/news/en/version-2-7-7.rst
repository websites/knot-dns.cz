Version 2.7.7
#############

:date: 2019-04-08
:lang: en

Improvements:
-------------
 - Possible zone transaction is aborted by zone events to avoid inconsistency
 - Added log message if no persistent config DB is available during 'conf-begin'
 - Tiny building improvements

Bugfixes:
---------
 - Glue records under delegation are sometimes signed
 - NSEC3 not re-salted during AXFR refresh
 - Broken NSEC3 chain after adding new sub-delegations
 - Failed to sign new zone contents if added dynamically #641
 - NSEC3 opt-out signing doesn't work in some cases
 - Redundant SOA RRSIG on slave if RRSIG TTL changed on master
 - Sometimes confusing log error message for NOTIFY event
 - Failed to explicit set value 0 for submission timeout
