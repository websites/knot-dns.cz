Version 2.5.2 and 2.4.5
#######################

:date: 2017-06-23
:lang: en

Knot DNS 2.5.2
==============
       
Security:
---------
 - Improper TSIG validity period check can allow TSIG forgery (Thanks to Synacktiv!)

Improvements:
-------------
 - Extended debug logging for TSIG errors
 - Better error message for unknown module section in the configuration
 - Module documentation compilation no longer depends on module configuration
 - Extended policy section configuration semantic checks
 - Improved python version compatibility in pykeymgr
 - Extended migration section in the documentation
 - Improved DNSSEC event timing on 32-bit systems
 - New KSK rollover start log info message
 - NULL qtype support in kdig

Bugfixes:
---------
 - Failed to process included configuration
 - dnskey_ttl policy option in the configuration has no effect on DNSKEY TTL
 - Corner case journal fixes (huge changesets, OpenWRT operation)
 - Confusing event timestamps in knotc zone-status output
 - NSEC/NSEC3 bitmap not updated for CDS/CDNSKEY
 - CDS/CDNSKEY RRSIG not updated

`Full Knot DNS 2.5.2 changelog <https://gitlab.nic.cz/knot/knot-dns/raw/v2.5.2/NEWS>`_

Knot DNS 2.4.5
==============

Security:
---------
 - Improper TSIG validity period check can allow TSIG forgery (Thanks to Synacktiv!)

Bugfixes:
---------
 - Corner case journal fixes (huge changesets, OpenWRT operation)

`Full Knot DNS 2.4.5 changelog <https://gitlab.nic.cz/knot/knot-dns/raw/v2.4.5/NEWS>`_
