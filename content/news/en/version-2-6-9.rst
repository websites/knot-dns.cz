Version 2.6.9
#############

:date: 2018-08-13
:lang: en

Improvements:
-------------
 - Added zone wire size to zone loading log message
 - Added debug log message for each unsuccessful remote address operation

Bugfixes:
---------
 - Zone not flushed after re-signing during zone load #594
 - Server crashes when committing empty zone transaction
 - Incoming IXFR with on-slave signing sometimes leads to memory corruption #595
