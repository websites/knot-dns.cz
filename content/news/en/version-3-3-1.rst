Version 3.3.1
#############

:date: 2023-09-11
:lang: en

Improvements:
-------------
 - knotd: multiple catalog groups per member are tolerated, but only one is used
 - modules: added const qualifier to various function parameters #877 (Thanks to Robert Edmonds)
 - libs: upgraded embedded libngtcp2 to 0.19.1

Bugfixes:
---------
 - knotd: TCP over XDP fails to respond
 - knotd: server can crash when adjusting a wildcard glue
 - knotd: failed to forward DDNS if 'zone.master' points to 'remotes'
 - knotd: broken YAML statistics if more modules are configured #874
 - knotd: DDNS forwarding isn't RFC 8945 compliant
