Version 2.5.1
#############

:date: 2017-06-07
:lang: en

Bugfixes:
---------
 - pykeymgr no longer crash on empty json files in the KASP DB directory
 - pykeymgr no longer imports keys in the "removed" state
 - Imported keys in the "removed" state no longer makes knotd to crash
 - Including an empty configuration directory no longer makes knotd to crash
 - pykeymgr is distributed and installed to the distribution tarball

`Full Knot DNS 2.5.1 changelog <https://gitlab.nic.cz/knot/knot-dns/raw/v2.5.1/NEWS>`_
