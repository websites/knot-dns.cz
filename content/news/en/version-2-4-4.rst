Version 2.4.4
#############

:date: 2017-06-04
:lang: en

Improvements:
-------------
 - Improved error handling in kjournalprint

Bugfixes:
---------
 - Zone flush not replanned upon unsuccessful flush
 - Journal inconsistency after deleting deleted zone
 - Zone events not rescheduled upon server reload (Thanks to Mark Warren)
 - Unreliable LMDB mapsize detection in kjournalprint
 - Some minor issues found by AddressSanitizer

`Full Knot DNS 2.4.4 changelog <https://gitlab.nic.cz/knot/knot-dns/raw/v2.4.4/NEWS>`_
