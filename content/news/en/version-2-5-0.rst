Version 2.5.0
#############

:date: 2017-06-05
:lang: en

Features:
---------
 - KASP database switched from JSON files to LMDB database
 - KSK rollover support using CDNSKEY and CDS in the automatic DNSSEC signing
 - Dynamic module loading support with proper module API
 - Journal can store full zone contents (not only differences)
 - Zone freeze/thaw support
 - Updated knotc zone-status output with optional column filters
 - New '[no]crypto' option in kdig
 - New keymgr implementation reflecting KASP database changes
 - New pykeymgr for JSON-based KASP database migration
 - Removed obsolete knot1to2 utility

Improvements:
-------------
 - Added libidn2 support to kdig (with libidn fallback)
 - Maximum timer database switched from configure to the server configuration

`Full Knot DNS 2.5.0 changelog <https://gitlab.nic.cz/knot/knot-dns/raw/v2.5.0/NEWS>`_
