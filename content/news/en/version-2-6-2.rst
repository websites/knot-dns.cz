Version 2.6.2
#############

:date: 2017-11-23
:lang: en

Features:
---------
 - CSK algorithm rollover and (KSK, ZSK) <-> CSK rollover support

Improvements:
-------------
 - Allowed explicit configuration for infinite ksk lifetime
 - Proper error messages instead of unclear error codes in server log
 - Better support for old compilers

Bugfixes:
---------
 - Unexpected reply for DS query with an owner below a delegation point
 - Old dependencies in the pkg-config file
