Version 2.7.8
#############

:date: 2019-07-15
:lang: en

Improvements:
-------------
 - Various improvements in the documentation

Bugfixes:
---------
 - Excessive server load when maximum TCP clients limit is reached
 - Incorrect reply after zone update with a node changed from non-authoritative to delegation
 - Missing debug log for failed zone refresh triggered by zone notification
 - Wrong processing of multiple $INCLUDE directives #646
 - Broken unit test on NetBSD 8.x
