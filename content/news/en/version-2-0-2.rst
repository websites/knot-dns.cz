Version 2.0.2
#############

:author: Jan Včelák
:date: 2015-11-24
:lang: en

Bugfixes:
~~~~~~~~~
* Out-of-bound read in packet parser for malformed NAPTR records

`Full Knot DNS 2.0 changelog <https://gitlab.nic.cz/knot/knot-dns/raw/v2.0.2/NEWS>`_
