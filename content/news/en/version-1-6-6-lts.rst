Version 1.6.6 (LTS)
###################

:author: Jan Včelák
:date: 2015-11-24
:lang: en

Bugfixes:
~~~~~~~~~
* Fix daemon startup systemd notification
* Out-of-bound read in packet parser for malformed NAPTR records

Features:
~~~~~~~~~
* Backported rosedb module

`Full Knot DNS 1.6 changelog <https://gitlab.nic.cz/knot/knot-dns/raw/v1.6.6/NEWS>`_
