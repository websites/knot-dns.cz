Version 3.0.10
##############

:date: 2021-11-03
:lang: en

Improvements:
-------------
 - doc: various fixes and improvements

Bugfixes:
---------
 - knotd: server can crash when receiving queries with NSID EDNS flag #774 (Thanks to Romain Labolle)
 - knotd: ds-push fails to update the parent zone if a CNAME exists for a non-terminal node
 - knotd: server crashes on reload when no interfaces configured #770
 - knotd: journal not able to free up space when zone-in-journal present and zonefile written
 - knotd: broken AXFR with knot as slave and dnsmasq as master (Thanks to Daniel Gröber)
 - knotd: server can crash when zone difference is inconsistent upon cold start
 - mod-stats: missing protocol counters for TCP over XDP
 - kzonesign: input zone name not lower-cased
