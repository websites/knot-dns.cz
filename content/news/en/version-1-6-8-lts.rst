Version 1.6.8 (LTS)
###################

:date: 2016-08-08
:lang: en

Features:
~~~~~~~~~
* Zone size limit restriction for DDNS, AXFR, and IXFR (CVE-2016-6171)

`Full Knot DNS 1.6 changelog <https://gitlab.nic.cz/knot/knot-dns/raw/v1.6.8/NEWS>`_
