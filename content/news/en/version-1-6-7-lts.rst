Version 1.6.7 (LTS)
###################

:date: 2016-02-09
:lang: en

Improvements:
~~~~~~~~~~~~~
* IXFR: Log change of the zone serial number after the transfer
* RRL: Document operational impact of various settings
* RRL: Add support for zero slip (dropping of all limited responses)
* Added ‘timer-db’ configuration option allowing relocation of timer database

`Full Knot DNS 1.6 changelog <https://gitlab.nic.cz/knot/knot-dns/raw/v1.6.7/NEWS>`_
