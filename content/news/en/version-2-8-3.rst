Version 2.8.3
#############

:date: 2019-07-16
:lang: en

Features:
---------
 - Added cert/key file configuration for TLS in kdig (Thanks to Alexander Schultz)

Improvements:
-------------
 - More verbose log message for offline-KSK signing
 - Module RRL logs affected source address subnet instead of only one source address
 - Extended DNSSEC policy configuration checks
 - Various improvements in the documentation

Bugfixes:
---------
 - Excessive server load when maximum TCP clients limit is reached
 - Incorrect reply after zone update with a node changed from non-authoritative to delegation
 - Wrong error line number in a config file if it contains leading tab character
 - Config file error message contains unrelated parsing context
 - NSEC3 salt not updated when reconfigured to zero length
 - Kjournalprint sometimes prints a random value for per-zone occupation
 - Missing debug log for failed zone refresh triggered by zone notification
 - DS check not scheduled when reconfigured
 - Broken unit test on NetBSD 8.x
