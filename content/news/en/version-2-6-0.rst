Version 2.6.0
#############

:date: 2017-09-29
:lang: en

Features:
---------
 - On-slave (inline) signing support
 - Automatic DNSSEC key algorithm rollover
 - Ed25519 algorithm support in DNSSEC (requires GnuTLS 3.6.0)
 - New 'journal-content' and 'zonefile-load' configuration options
 - keymgr tries to run as user/group set in the configuration
 - Public-only DNSSEC key import into KASP DB via keymgr
 - NSEC3 resalt and parent DS query events are persistent in timer DB
 - New processing state for a response suppression within a query module
 - Enabled server side TCP Fast Open if supported
 - TCP Fast Open support in kdig

Improvements:
-------------
 - Better record owner compression if related to the previous rdata dname
 - NSEC(3) chain is no longer recomputed whole on every update
 - Remove inconsistent and unnecessary quoting in log files
 - Avoiding of overlapping key rollovers at a time
 - More DNSSSEC-related semantic checks
 - Extended timestamp format in keymgr

Bugfixes:
---------
 - Incorrect journal free space computation causing inefficient space handling
 - Interface-automatic broken on Linux in the presence of asymmetric routing
