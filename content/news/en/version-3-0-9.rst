Version 3.0.9
#############

:date: 2021-09-07
:lang: en

Improvements:
-------------
 - keymgr: import-bind sets publish and active timers to now if missing timers #747

Bugfixes:
---------
 - knotd: incomplete NSEC3 proof in response to opt-outed empty non-terminal
 - knotd: journal discontinuity and zone-in-journal result in incorrectly calculated journal occupation
 - knotd: incorrect evaluation of ACL deny rule in combination with TSIG
 - knotd: failed DS-check is replanned even if no key is ready
 - knotd: root zone not correctly purged from the journal
 - kdig: +noall does not filter out AUTHORITY comment #749
