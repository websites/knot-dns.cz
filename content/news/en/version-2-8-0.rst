Version 2.8.0
#############

:date: 2019-03-05
:lang: en

Features:
---------
 - New offline-KSK mode of operation
 - Configurable multithreaded DNSSEC signing for large zones
 - Extended ACL configuration for dynamic updates
 - New knotc trigger 'zone-key-rollover' for immediate DNSKEY rollover
 - Added support for OPENPGPKEY, CSYNC, SMIMEA, and ZONEMD RR types
 - New 'double-ds' option for CDS/CDNSKEY publication

Improvements:
-------------
 - Significant speed-up of zone updates
 - Knotc supports force option in the interactive mode
 - Copy-on-write support for QP-trie (Thanks to Tony Finch)
 - Unified and more efficient LMDB layer for journal, timer, and KASP databases
 - DS check event is re-planned according to KASP even when purged timers
 - Module DNS Cookies supports explicit Server Secret configuration
 - Zone mtime is verified against full-precision timestamp (Thanks to Daniel Kahn Gillmor)
 - Extended logging (loaded SOA serials, refresh duration, tiny cleanup)
 - Relaxed fixed-length condition for DNSSEC key ID
 - Extended semantic checks for DNAME and NS RR types
 - Added support for FreeBSD's SO_REUSEPORT_LB
 - Improved performance of geoip module
 - Various improvements in the documentation

Compatibility:
--------------
 - Changed configuration default for 'cds-cdnskey-publish' to 'rollover'
 - Journal DB format changes are not downgrade-compatible
 - Keymgr no longer prints DS for algorithm SHA-1
