Version 2.1.0-rc1
#################

:date: 2015-12-20
:author: Jan Včelák
:lang: en

Features:
~~~~~~~~~
* Per-thread UDP socket binding using SO_REUSEPORT on Linux
* Support for dynamic configuration database
* DNSSEC: Support for cryptographic tokens via PKCS #11 interface
* DNSSEC: Experimental support for online signing

`Full Knot DNS 2.1 changelog <https://gitlab.nic.cz/knot/knot-dns/raw/v2.1.0-rc1/NEWS>`_
