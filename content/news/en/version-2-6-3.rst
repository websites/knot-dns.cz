Version 2.6.3
#############

:date: 2017-11-24
:lang: en

Bugfixes:
---------
 - Wrong detection of signing scheme rollover
