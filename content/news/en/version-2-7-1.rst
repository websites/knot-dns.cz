Version 2.7.1
#############

:date: 2018-08-14
:lang: en

Improvements:
-------------
 - Added zone wire size information to zone loading log message
 - Added debug log message for each unsuccessful remote address operation
 - Various improvements for packaging

Bugfixes:
---------
 - Incompatible handling of RRSIG TTL value when creating a DNS message
 - Incorrect RRSIG TTL value in zone differences and knotc zone operation outputs
 - Default configure prefix is ignored
