Version 2.1.1
#############

:date: 2016-02-10
:lang: en

Improvements:
~~~~~~~~~~~~~
* Select correct source address for UDP messages recieved on ANY address
* Extend documentation of knotc commands

Bugfixes:
~~~~~~~~~
* DNSSEC: Allow import of duplicate private key into the KASP
* DNSSEC: Avoid duplicate NSEC for Wildcard No Data answer
* Fix server crash when an incomming transfer is in progress and reload is issued
* Fix socket polling when configured with many interfaces and threads
* Fix compilation against Nettle 3.2

`Full Knot DNS 2.1 changelog <https://gitlab.nic.cz/knot/knot-dns/raw/v2.1.1/NEWS>`_
