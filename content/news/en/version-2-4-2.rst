Version 2.4.2
#############

:date: 2017-03-23
:lang: en

Features:
---------
 - Zscanner can store record comments placed on the same line
 - Knotc status extension with version, configure, and workers parameters

Improvements:
-------------
 - Significant incoming XFR speed-up in the case of many zones

Bugfixes:
---------
 - Lower serial at master didn't trigger any errors
 - Queries with too long DNAME substitution did not return YXDOMAIN response
 - Incorrect elapsed time in the DDNS log
 - Failed to process forwarded DDNS request with TSIG

`Full Knot DNS 2.4.2 changelog <https://gitlab.nic.cz/knot/knot-dns/raw/v2.4.2/NEWS>`_
