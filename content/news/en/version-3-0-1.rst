Version 3.0.1
#############

:date: 2020-10-10
:lang: en

Features:
---------
 - New command in keymgr for validation of RRSIGs in SKR
 - Keymgr validates RRSIGs in SKR during import
 - New option in kzonecheck to skip DNSSEC-related checks

Improvements:
-------------
 - Module noudp has new configuration option for UDP truncation rate
 - Better detection of reproducible signing availability
 - Kxdpgun allows setting of network interface
 - Default control timeout in knotc was increased to 60 seconds
 - DNSSEC validation searches for invalid redundant RRSIGs
 - Configuration source detection no longer considers empty confdb directory as active configuration
 - Zone backup preserves original zone file if zone file synchronization is disabled

Bugfixes:
---------
 - NSEC3 re-salt can cause server crash due to possible zone inconsistencies
 - Zone reload logs 'invalid parameter' if zone file not changed
 - Outgoing multi-message transfer can contain invalid compression pointers under specific conditions
 - Improper handling of file descriptors in libdnssec
 - Server crashes if no policy is configured with DNSSEC validation
 - Server crashes if DNSSEC validation is enabled for unsigned zone
 - Failed to build with libnghttp2 (Thanks to Robert Edmonds)
 - Various bugs in zone data backup/restore
