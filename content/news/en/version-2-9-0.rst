Version 2.9.0
#############

:date: 2019-10-10
:lang: en

Features:
---------
 - Full support for different master/slave serial arithmetics when on-slave signing
 - Module geoip newly supports wildcard records #650
 - New DNSSEC policy configuration option 'rrsig-pre-refresh' for reducing
   frequency of the zone signing event
 - New server configuration option 'tcp-reuseport' for setting SO_REUSEPORT(_LB)
   mode on TCP sockets
 - New server configuration option 'tcp-io-timeout' [ms] for restricting inbound
   IO operations over TCP #474

Improvements:
-------------
 - Significant speed-up of zone contents modifications
 - Avoided double zone signing during CSK rollovers
 - Self-created RRSIGs are not cryptographically verified if not necessary
 - Zone journal can store two changesets if zone file difference computing
   and DNSSEC signing are enabled. The first one containing the difference of
   zone history needed by slave servers, the second one containing the difference
   between zone file and zone needed for server restart
 - Universal and more robust memory clearing
 - More precise socket timeout handling
 - New notice log message for configuration changes requiring server restart
 - Module RRL logs both trigger source address and affected subnet
 - Various code (especially zone and TCP processing) and documentation improvements

Bugfixes:
---------
 - RRSIGs are wrongly checked for inconsistent RRSet TTLs during zone update
 - DS check/push warnings after disabled DNSSEC signing
 - NSEC3 records not accessible through control interface
 - Module geoip doesn't accept underscore character in dname specification #655

Compatibility:
--------------
 - Removed runtime reconfiguration of network workers and interfaces since
   it was imperfect and also couldn't work after dropped process privileges
 - Removed inaccurate and misleading knotc command 'zone-memstats' because
   memory consumption varies during zone modifications or transfers
 - Removed useless 'zone.request-edns-option' configuration option
 - Reimplemented DNS Cookies to be interoperable (based on draft-ietf-dnsop-server-cookies
   and work by Witold Kręcicki)
 - Default limit on TCP clients is auto-configured to one half of the file
   descriptor limit for the server process
 - Number of open files limit is set to 1048576 in upstream packages
 - Default number of TCP workers is equal to the number of online CPUs or at least 10
 - Default EDNS buffer size is 1232 for both IPv4 and IPv6
 - Removed 'tcp-handshake-timeout' server configuration option
 - Some configuration options were renamed and possibly moved. Old names will
   be supported at least until next major release:

   - 'server.tcp-reply-timeout' [s] to 'server.tcp-remote-io-timeout' [ms]
   - 'server.max-tcp-clients'       to 'server.tcp-max-clients'
   - 'server.max-udp-payload'       to 'server.udp-max-payload'
   - 'server.max-ipv4-udp-payload'  to 'server.udp-max-payload-ipv4'
   - 'server.max-ipv6-udp-payload'  to 'server.udp-max-payload-ipv6'
   - 'template.journal-db'          to 'database.journal-db'
   - 'template.journal-db-mode'     to 'database.journal-db-mode'
   - 'template.max-journal-db-size' to 'database.journal-db-max-size'
   - 'template.kasp-db'             to 'database.kasp-db'
   - 'template.max-kasp-db-size'    to 'database.kasp-db-max-size'
   - 'template.timer-db'            to 'database.timer-db'
   - 'template.max-timer-db-size'   to 'database.timer-db-max-size'
   - 'zone.max-journal-usage'       to 'zone.journal-max-usage'
   - 'zone.max-journal-depth'       to 'zone.journal-max-depth'
   - 'zone.max-zone-size'           to 'zone.zone-max-size'
   - 'zone.max-refresh-interval'    to 'zone.refresh-max-interval'
   - 'zone.min-refresh-interval'    to 'zone.refresh-min-interval'
