Version 2.9.3
#############

:date: 2020-03-03
:lang: en

Features:
---------
 - New configuration option 'remote.block-notify-after-transfer' to suppress
   sending NOTIFY messages
 - Enabled testing support for Ed448 DNSSEC algorithm (requires GnuTLS 3.6.12+
   and not-yet-released Nettle 3.6+)
 - New keymgr parameter 'local-serial' for getting/setting signed zone SOA serial
   in the KASP database
 - keymgr can import Ed25519 and Ed448 keys in the BIND format (Thanks to Conrad Hoffmann)

Improvements:
-------------
 - kdig returns error if the query name is invalid
 - Increased 'server.tcp-io-timeout' default value to 500 ms
 - Decreased 'database.journal-db-max-size' default value to 512 MiB on 32-bit systems
 - Server no longer falls back to AXFR if master is outdated during zone refresh
 - Some documentation improvements (including new EPUB format and compatibility
   with Ultra Electronics CIS Keyper Plus HSM)
 - Some packaging improvements (including new python3-libknot deb package)

Bugfixes:
---------
 - Outgoing IXFR can be malformed if the message size has specific size
 - Server can crash if the zone contains solo NSEC3 record
 - Improved compatibility with older journal format
 - Incorrect SOA TTL in negative answers — SOA minimum not considered
 - Cannot unset uppercase nodes via control interface #668
 - Module RRL doesn't set AA flag and NOERROR rcode in slipped responses
 - Server returns FORMERR instead of NOTIMP if empty QUESTION and unknown OPCODE
