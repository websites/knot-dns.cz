Version 2.3.1
#############

:date: 2016-10-10
:lang: en

Bugfixes:
~~~~~~~~~
* Missing glue records in some responses
* Knsupdate prompt printing on non-terminal
* Mismatch between configuration policy item names and documentation
* Segfault on OS X (Sierra)

Improvements:
~~~~~~~~~~~~~
* Significant speed-up of conf-commit and conf-diff operations (in most cases)
* New EDNS Client Subnet libknot API
* Better semantic-checks error messages

Features:
~~~~~~~~~
* Print TLS certificate hierarchy in kdig verbose mode
* New +subnet alias for +client
* New mod-whoami and mod-noudp modules
* New zone-purge control command
* New log-queries and log-responses options for mod-dnstap

`Full Knot DNS 2.3.1 changelog <https://gitlab.nic.cz/knot/knot-dns/raw/v2.3.1/NEWS>`_
