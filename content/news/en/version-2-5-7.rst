Version 2.5.7
#############

:date: 2018-01-01
:lang: en

Bugfixes:
---------
 - Unintentional zone re-sign during reload if empty NSEC3 salt
 - Inconsistent zone names in journald structured logs
 - Malformed outgoing transfer for big zone with TSIG
 - Unexpected reply for DS query with an owner below a delegation point
 - Old dependencies in the pkg-config file
