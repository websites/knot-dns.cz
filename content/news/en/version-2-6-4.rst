Version 2.6.4
#############

:date: 2018-01-02
:lang: en

Features:
---------
 - Module synthrecord allows multiple 'network' specification
 - New CSK handling support in keymgr

Improvements:
-------------
 - Allowed configuration for infinite zsk lifetime
 - Increased performance and security of the module synthrecord
 - Signing changeset is stored into journal even if 'zonefile-load' is whole

Bugfixes:
---------
 - Unintentional zone re-sign during reload if empty NSEC3 salt
 - Inconsistent zone names in journald structured logs
 - Malformed outgoing transfer for big zone with TSIG
 - Some minor DNSSEC-related issues
