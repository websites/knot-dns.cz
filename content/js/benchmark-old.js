var KNOT = {};
KNOT.rawChartData = [];
KNOT.charts = {};
KNOT.chartType = {};
KNOT.hiddenSeriesNames = [];
KNOT.propertyDict = {
    os: 'Operating system',
    deployment: 'Deployment',
    adapter: 'Network adapter'
};
KNOT.langPreferences = {
    decimalPoint: '.',
    thousandsSep: ' '
};
KNOT.units = {
    time: 's',
    memory: 'MB'
};
KNOT.colors = {
    server: {
        // blue, knot-dns.cz style!
        'Knot DNS': ['#003e84', '#02aec4', '#0088cc', '#3366ff', '#66ccff', '#333399'],
        // red
        'YADIFA': ['#990000', '#cc3333', '#ff0033'],
        // purple
        'BIND': ['#660099', '#cc3399', '#990099'],
        // green
        'NSD': ['#99cc33', '#1b7159', '#cccc66'],
        // orange
        'PowerDNS': ['#e17f03', '#ffcc33', '#ff9966']
    },
    stack: {
        time: ['#0088cc', '#003e84', '#02aec4', '#3366ff', '#66ccff'],
        memory: ['#990000', '#cc3333', '#ff0033', '#990099']
    },
    // grey scale for unknown servers
    other: ['#202020', '#A0A0A0', '#606060'],
    named: {
        'small zone (unsigned)': '#02aec4',
        'small zone (DNSSEC)': '#003e84',
        'large zone (unsigned)': '#cc3333',
        'large zone (DNSSEC)': '#990000'
    }
};
KNOT.markers = {
    server: {
        'Knot DNS': 'circle',
        'YADIFA': 'square',
        'BIND': 'triangle-down',
        'NSD': 'triangle'
    },
    other: 'diamond',
    named: {}
};
KNOT.dashStyles = [
    'ShortDash',
    'ShortDot',
    'ShortDashDot',
    'ShortDashDotDot',
    'Solid',
    'Dot',
    'Dash',
    'LongDash',
    'DashDot',
    'LongDashDot',
    'LongDashDotDot'
];
/*
 * default chart options extending default Highcharts.Chart().options
 * chart type is 'line' by default
 */
KNOT.chartType.defaultChart = Highcharts.setOptions({
    chart: {
        animation: false,
        // related is not a Highcharts property
        // array of related chartOptions IDs
        related: []
    },
    credits: {
        enabled: false
    },
    title: {
        text: ''
    },
    plotOptions: {
        series: {
            animation: false,
            events: {
                legendItemClick: function () {
                    KNOT.handleSeriesVisibility(this);
                }
            }
        }
    },
    series: []
});
// response rate line chart options
KNOT.chartType.responseRatePercentage = jQuery.extend(true, {}, KNOT.chartType.defaultChart, {
    chart: {
        type: 'spline',
        related: ['response-rate', 'response-rate-total'],
        relatedTitle: 'Response Rate'
    },
    title: {
        text: 'Response Rate - Percentage'
    },
    xAxis: {
        title: {
            text: 'Queries per second'
        }
    },
    yAxis: {
        max: 100,
        min: 0,
        title: {
            text: 'Response Rate [%]'
        }
    },
    tooltip: {
        formatter: function () {
            return '<span style="color:' + this.series.color + '">' + this.series.name + '</span><br/>' +
                   'Queries per second: ' + KNOT.hchNumFormat(this.x) + '<br/>Response rate: ' +
                   KNOT.hchNumFormat(this.y, 1) + ' %';
        }
    }
});
KNOT.chartType.responseRateTotal = jQuery.extend(true, {}, KNOT.chartType.responseRatePercentage, {
    title: {
        text: 'Response Rate'
    },
    yAxis: {
        min: 0,
        max: null,
        title: {
            text: 'Answers per second'
        }
    },
    tooltip: {
        formatter: function () {
            return '<span style="color:' + this.series.color + '">' + this.series.name + '</span><br/>' +
                   'Queries per second: ' + KNOT.hchNumFormat(this.x) + '<br/>Response rate: ' +
                   KNOT.hchNumFormat(this.y);
        }
    }
});
KNOT.chartType.column = jQuery.extend(true, {}, KNOT.chartType.defaultChart, {
    chart: {
        type: 'column'
    },
    xAxis: {
        categories: [],
        labels: {
            style: {
                color: '#333'
            }
        }
    }
});
// resource usage column chart options
KNOT.chartType.resourceUsage = jQuery.extend(true, {}, KNOT.chartType.column, {
    title: {
        text: 'Startup Time and Memory Usage'
    },
    plotOptions: {
        series: {
            stacking: 'normal',
            dataLabels: {
                enabled: true,
                inside: false,
                color: '#222',
                formatter: function () {
                    if (this.point.stackTotal === this.point.stackY && this.y > 0) {
                        return this.total + ' ' + KNOT.units[this.series.options.stack];
                    } else {
                        return '';
                    }
                }
            }
        }
    },
    xAxis: {
        gridLineWidth: 0.7,
        gridLineDashStyle: 'dot',
        offset: -30
    },
    yAxis: [{
        top: 50,
        height: 150,
        offset: 0,
        labels: {
            formatter: function () {
                return this.value + ' s';
            }
        },
        title: {
            text: 'Startup time [s]'
        }
    }, {
        height: 150,
        top: 220,
        offset: 0,
        labels: {
            formatter: function () {
                return this.value + ' MB';
            }
        },
        title: {
            text: 'Memory Usage [MB]'
        }
    }],
    tooltip: {
        headerFormat: '<span style="font-size: 12px; font-weight: bold">{point.key}</span><br/>',
        shared: true
    }
});
KNOT.chartType.ddnsQueryProcessing = jQuery.extend(true, {}, KNOT.chartType.column, {
    title: {
        text: 'Dynamic DNS query processing time'
    },
    yAxis: {
        labels: {
            format: '{value} s'
        },
        title: {
            text: 'Processing time [s]'
        }
    },
    tooltip: {
        valueSuffix: ' s',
        headerFormat: '<span style="font-size: 12px; font-weight: bold">{point.key}</span><br/>',
        shared: true
    }
});
KNOT.chartOptions = {
    'response-rate-total': KNOT.chartType.responseRateTotal,
    'response-rate': KNOT.chartType.responseRatePercentage,
    'resource-usage': KNOT.chartType.resourceUsage,
    'ddns-query-processing': KNOT.chartType.ddnsQueryProcessing
};

KNOT.Chart = {
    init: function (id, data) {
        this.id = id;
        this.highchartId = this.id + '-chart';
        this.parent = jQuery('#' + this.id + '-benchmark');
        this.related = KNOT.chartOptions[id].chart.related;
        this.properties = {};
        this.chartData = {};
        this.propertyNames = [];
        this.activeProperties = {};
        this.highchart = null;
        this.rawCharts = data;
        this.serieNames = [];
        this.stacks = [];
        this.adapterFilterActive = false;
        this.createContainer();
        this.initProperties();
        this.createFilter();
        this.adapterFilter();
//        this.filterTipBox();
        this.initChart();
    },
    createContainer: function () {
        this.parent.append(jQuery('<div/>', {
            id: this.id
        }));
    },
    initProperties: function () {
        // assume all Chart data have the same property names, use first as default
        for (var propertyName in this.rawCharts[0].properties) {
            this.properties[propertyName] = [];
            this.propertyNames.push(propertyName);
        }
        this.activeProperties = deepcopy(this.rawCharts[0].properties);
        for (var property in this.properties) {
            for (var i = 0, rcLength = this.rawCharts.length; i < rcLength; i++) {
                var val = this.rawCharts[i].properties[property];
                if (val) {
                    if (this.properties[property].indexOf(val) < 0) {
                        this.properties[property].push(val);
                    }
                }
                for (var j = 0, sLength = this.rawCharts[i].series.length; j < sLength; j++) {
                    var serie = this.rawCharts[i].series[j];
                    if (this.serieNames.indexOf(serie.name) < 0) {
                        this.serieNames.push(serie.name);
                    }
                }
            }
        }
    },
    createFilter: function () {
        var $container = jQuery('<table/>', {
            'class': 'filter'
        });
        for (var property in this.properties) {
            var $row = jQuery('<tr/>'),
                title = (KNOT.propertyDict[property]) ? KNOT.propertyDict[property] : property,
                $title = jQuery('<th/>', {
                    html: title + ':'
                }),
                $property = jQuery('<td/>', {
                    'id': this.id + '_prop_' + property
                }),
                values = this.properties[property];
            for (var i = 0, vLength = values.length; i < vLength; i++) {
                var val = values[i],
                    id = this.id + '_check_' + property + '_val_' + val,
                    $label = jQuery('<label/>', {
                        'html': val,
                        'for': id
                    }),
                    $input = jQuery('<input/>', {
                        'id': id,
                        'type': 'radio',
                        'name': this.id + '_' + property,
                        'value': val
                    });
                if (this.activeProperties[property] === val) {
                    $input.attr('checked', 'checked');
                }
                $input.change(this._updateChart.bind(this));
                $property.append($input).append($label);
            }
            $row.append($title).append($property);
            $container.append($row);
            jQuery('#' + this.id).append($container);
        }
    },
    adapterFilter: function () {
        if (this.properties.adapter) {
            this._createAdapterButtons();
            this._adapterFilterSwitchers();
        }
    },
    filterTipBox: function () {
        var $row = jQuery('<tr/>'),
            $cell = jQuery('<td/>', {
                'colspan': 2,
                'class': 'tip',
                'html': 'Select desired test configuration above. You can also click on legend items below chart ' +
                        'to hide data series.'
            });
        $row.append($cell);
        jQuery('#' + this.id + ' .filter').append($row);
    },
    initChart: function () {
        if (this.rawCharts[0].categories) {
            this._assignStackColors();
        } else {
            this._assignServerGraphics();
        }
        for (var i = 0, rcLength = this.rawCharts.length; i < rcLength; i++) {
            var rawChart = this.rawCharts[i],
                hash = this._propertyHash(rawChart.properties);
            this._handleStacked(rawChart.series);
            if (rawChart.subcategories) {
                this.chartData[hash] = this._loadSubcategories(rawChart);
            } else {
                this.chartData[hash] = rawChart;
            }
            this.chartData[hash].series = this._setGraphics(rawChart.series);
        }
        this._loadChartHtml();
        if (this.related.length >= 1) {
            this.initRelated();
        }
        this.reload();
    },
    initRelated: function () {
        this.relatedData = {};
        this.relatedHighcharts = {};
        var $wrapper = jQuery('#' + this.id + '-chart-wrapper'),
            $tabs = this._createRelatedTabs();
        $wrapper.prepend($tabs);
        for (var i = 0, rLength = this.related.length; i < rLength; i++) {
            var id = this.related[i];
            this.relatedHighcharts[id] = null;
            this.relatedData[id] = deepcopy(this.chartData);
            if (id !== this.id) {
                for (var hash in this.chartData) {
                    var series = this.chartData[hash].series;
                    // hard-coded only one data transformation function
                    this.relatedData[id][hash].series = this._totalFromProportional(series);
                }
                $wrapper.append(jQuery('<div/>', {
                    'id': id + '-chart',
                    'class': 'chart',
                    'style': 'display:none'
                }));
            }
        }
    },
    _createRelatedTabs: function () {
        var _this = this,
            $tabs = jQuery('<ul/>', {
                'class': 'nav-pills tabs vertical-tabs'
            });
        for (var i = 0, rLength = this.related.length; i < rLength; i++) {
            var id = this.related[i],
                title = KNOT.chartOptions[id].title.text,
                $tab = jQuery('<li/>', {
                    'html': title,
                    'id': 'tab-vertical-' + id,
                    'value': i
                });
            if (i === 0) {
                $tab.addClass('active');
            }
            $tabs.append($tab);
            $tab.click(function () {
                var $tab = jQuery(this),
                    index = $tab.attr('value'),
                    id = _this.related[index];
                if ($tab.hasClass('active')) {
                    return;
                } else {
                    $tab.siblings().removeClass('active');
                    $tab.addClass('active');
                    jQuery('#' + _this.highchartId).hide();
                    _this.highchartId = id + '-chart';
                    _this.highchart = _this.relatedHighcharts[id];
                    jQuery('#' + _this.highchartId).show();
                    _this.chartData = _this.relatedData[id];
                    _this.reload(id);
                    if (_this.adapterFilterActive) {
                        jQuery('#adapters-all-button').click();
                    }
                }
            });
        }
        return $tabs;
    },
    reload: function (chartId) {
        chartId = pick(chartId, this.id);
        var chart = this.chartData[this._propertyHash(this.activeProperties)];
        if (this.highchart) {
            this.removeSeries(false);
            if (chart.categories) {
                this.highchart.xAxis[0].setCategories(chart.categories);
            }
        } else {
            chart.options = KNOT.chartOptions[chartId];
            chart.options.chart.renderTo = this.highchartId;
            if (chart.categories) {
                chart.options.xAxis.categories = chart.categories;
            }
            this.highchart = new Highcharts.Chart(chart.options);
        }
        this.highchart.setTitle({}, {
            text: this._createSubtitle()
        });
        this.addSeries(chart.series);
        this.setNote(chart.note);
    },
    removeSeries: function (redraw) {
        while (this.highchart.series.length > 0) {
            this.highchart.series[0].remove(false);
        }
        redraw = pick(redraw, true);
        if (redraw) {
            this.highchart.redraw();
        }
    },
    addSeries: function (series) {
        for (var i = 0, sLength = series.length; i < sLength; i++) {
            series[i].visible = KNOT.hiddenSeriesNames.indexOf(series[i].name) < 0;
            series[i].animation = false;
            this.highchart.addSeries(series[i], false, false);
        }
        this.highchart.redraw();
    },
    setNote: function (note) {
        note = pick(note, '');
        jQuery('#' + this.id + ' .note').hide(0).html(note).fadeIn(1000);
    },
    show: function () {
        this.parent.show();
    },
    hide: function () {
        this.parent.hide();
    },
    toggleInfo: function () {
        this.parent.children('*[id!="' + this.id + '"]').not('h2').toggle();
    },
    _loadChartHtml: function () {
        var $wrapper = jQuery('<div/>', {
            'id': this.id + '-chart-wrapper',
            'class': 'chart-wrapper'
        });
        $wrapper.append(jQuery('<div/>', {
            'id': this.highchartId,
            'class': 'chart'
        }));
        jQuery('#' + this.id).append($wrapper).append('<div class="note"></div>');
    },
    _assignServerGraphics: function () {
        for (var i = 0, svLength = this.serieNames.length; i < svLength; i++) {
            var name = this.serieNames[i];
            if (!KNOT.colors.named[name]) {
                var color = null;
                for (var server in KNOT.colors.server) {
                    if (name.substring(0, server.length) === server) {
                        if (KNOT.colors.server[server].length) {
                            color = KNOT.colors.server[server].shift();
                        }
                        break;
                    }
                }
                if (!color) {
                    if (KNOT.colors.other.length) {
                        color = KNOT.colors.other.shift();
                    }
                }
                KNOT.colors.named[name] = color;
            }
            if (!KNOT.markers.named[name]) {
                var marker = null;
                for (var server in KNOT.markers.server) {
                    if (name.substring(0, server.length) === server) {
                        if (KNOT.markers.server[server].length) {
                            marker = KNOT.markers.server[server];
                        }
                        break;
                    }
                }
                if (!marker) {
                    marker = KNOT.markers.other;
                }
                KNOT.markers.named[name] = marker;
            }
        }
    },
    _assignStackColors: function () {
        for (var i = 0, sLength = this.stacks.length; i < sLength; i++) {
            var style = {
                color: KNOT.colors.named[this.stacks[i]]
            };
            KNOT.chartOptions[this.id].yAxis[i].labels.style = style;
            KNOT.chartOptions[this.id].yAxis[i].title.style = style;
        }
    },
    _setGraphics: function (series) {
        var ret = [];
        for (var i = 0, sLength = series.length; i < sLength; i++) {
            var name = series[i].name;
            if (KNOT.colors.named[name]) {
                series[i].color = KNOT.colors.named[name];
            }
            if (KNOT.markers.named[name]) {
                series[i].marker = {
                    'symbol': KNOT.markers.named[name]
                };
            }
            if (name.toLowerCase().indexOf('knot') >= 0) {
                series[i].zIndex = 1;
            }
            ret.push(series[i]);
        }
        return ret;
    },
    _handleStacked: function (series) {
        for (var i = 0, sLength = series.length; i < sLength; i++) {
            if (series[i].stack) {
                var stack = series[i].stack,
                    name = series[i].name;
                if (this.stacks.indexOf(stack) < 0) {
                    this.stacks.push(stack);
                    KNOT.colors.named[stack] = KNOT.colors.stack[stack][0];
                }
                if (KNOT.units[stack]) {
                    series[i].tooltip = {
                        valueSuffix: ' ' + KNOT.units[stack]
                    };
                }
                series[i].yAxis = this.stacks.indexOf(stack);
                if (!KNOT.colors.named[name]) {
                    KNOT.colors.named[name] = KNOT.colors.stack[stack].shift();
                }
            } else {
                break;
            }
        }
    },
    _loadSubcategories: function (chartData) {
        var newCategories = [];
        for (var i = 0, cLength = chartData.categories.length; i < cLength; i++) {
            newCategories.push({
                name: chartData.categories[i],
                categories: chartData.subcategories
            });
        }
        chartData.categories = newCategories;
        return chartData;
    },
    _propertyHash: function (properties) {
        var hash = '';
        for (var i = 0, pnLength = this.propertyNames.length; i < pnLength; i++) {
            hash += properties[this.propertyNames[i]];
        }
        return hash;
    },
    _createAdapterButtons: function () {
        var $row = jQuery('<tr/>'),
            $header = jQuery('<th>Server:</th>'),
            $inputs = jQuery('<td/>', {
                'class': 'checkbox-adapter'
            });
        for (var i = 0, svLength = this.serieNames.length; i < svLength; i++) {
            var sv = this.serieNames[i],
                id = this.id + '_check_' + sv,
                $input = jQuery('<input>', {
                    'id': id,
                    'type': 'checkbox',
                    'value': sv
                }),
                $label = jQuery('<label/>', {
                    'html': sv,
                    'for': id
                });
            $input.change(this._updateAdapters.bind(this));
            $inputs.append($input).append($label);
        }
        $row.append($header).append($inputs);
        jQuery('#' + this.id + ' .filter').append($row);
        $row.hide();
    },
    _adapterFilterSwitchers: function () {
        var _this = this,
            $adaptersButton = jQuery('<button id="adapters-all-button">Compare all adapters</button>')
                                .click(function () {
                jQuery('#' + _this.id + '_prop_adapter').parent().hide();
                var $checkboxes = jQuery('#' + _this.id + ' .checkbox-adapter');
                $checkboxes.parent().show();
                _this.adapterFilterActive = true;
                if ($checkboxes.children('input:checked').length === 0) {
                    // initialize with first knot series
                    $checkboxes.children('input[id*="Knot"]').first().click();
                } else {
                    _this._updateAdapters();
                }
            }),
            $serversButton = jQuery('<button id="servers-all-button">Compare all servers</button>').click(function () {
                jQuery('#' + _this.id + '_prop_adapter').parent().show();
                jQuery('#' + _this.id + ' .checkbox-adapter').parent().hide();
                _this.adapterFilterActive = false;
                _this.reload();
            });
        jQuery('#' + this.id + '_prop_adapter').prev('th').append('<br>').append($adaptersButton);
        jQuery('#' + this.id + ' .checkbox-adapter').prev('th').append('<br>').append($serversButton);
    },
    _updateChart: function (event) {
        var nameArray = event.target.name.split('_');
        this.activeProperties[nameArray[nameArray.length - 1]] = event.target.value;
        if (this.adapterFilterActive) {
            this._updateAdapters();
        } else {
            this.reload();
        }
    },
    _updateAdapters: function () {
        this.removeSeries(false);
        var chartData = [],
            checkedServerNames = [],
            properties = deepcopy(this.activeProperties),
            newSeries = [];
        jQuery('#' + this.id + ' .checkbox-adapter input:checked').each(function () {
            checkedServerNames.push(this.value);
        });
        for (var ai = 0, aLength = this.properties.adapter.length; ai < aLength; ai++) {
            properties.adapter = this.properties.adapter[ai];
            chartData.push(this.chartData[this._propertyHash(properties)]);
        }
        for (var i = 0, cdLength = chartData.length; i < cdLength; i++) {
            var series = chartData[i].series;
            for (var si = 0, sLength = series.length; si < sLength; si++) {
                var dashStyle = KNOT.dashStyles[i];
                if (checkedServerNames.indexOf(series[si].name) >= 0) {
                    var newSerie = deepcopy(series[si]);
                    newSerie.name = newSerie.name + ' - ' + chartData[i].properties.adapter;
                    newSerie.dashStyle = dashStyle;
                    newSeries.push(newSerie);
                }
            }
        }
        this.highchart.options.legend.symbolWidth = 30;
        this.highchart.setTitle({}, {
            text: this._createSubtitle(['adapter'])
        });
        this.addSeries(newSeries);
    },
    /**
     * return series with total value of responses instead of percentage
     */
    _totalFromProportional: function (series) {
        var newSeries = [];
        for (var i = 0, sLength = series.length; i < sLength; i++) {
            var newSerie = deepcopy(series[i]);
            for (var j = 0, dLength = series[i].data.length; j < dLength; j++) {
                newSerie.data[j][1] = Math.round(newSerie.data[j][0] * newSerie.data[j][1] * 0.01);
            }
            newSeries.push(newSerie);
        }
        return newSeries;
    },
    _createSubtitle: function (exclude) {
        var subtitle = '',
            chartData = this.chartData[this._propertyHash(this.activeProperties)];
        exclude = pick(exclude, []);
        for (var i = 0, pnLength = this.propertyNames.length; i < pnLength; i++) {
            if (exclude.indexOf(this.propertyNames[i]) < 0) {
                subtitle += chartData.properties[this.propertyNames[i]] + ', ';
            }
        }
        subtitle += ' (' + chartData.date + ')';
        return subtitle;
    }
};
KNOT.initialize = function () {
    var data = this.groupedRawData();
    this.initCharts(data);
    this.createTabs();
};
KNOT.initCharts = function (chartData) {
    for (var chartId in chartData) {
        this.charts[chartId] = deepcopy(KNOT.Chart);
        this.charts[chartId].init(chartId, chartData[chartId]);
    }
};
KNOT.groupedRawData = function () {
    var data = {};
    for (var i = 0, rcdLength = this.rawChartData.length; i < rcdLength; i++) {
        // rawChartData.type corresponds to id used in HTML, in chartOptions and charts
        var id = this.rawChartData[i].type;
        if (data[id]) {
            data[id].push(this.rawChartData[i]);
        } else {
            data[id] = [this.rawChartData[i]];
        }
    }
    return data;
};
KNOT.setTabHash = function (id) {
    window.location.hash = 'tab-' + id;
};
KNOT.createTabs = function () {
    var $tabs = jQuery('#benchmark-tabs'),
        items = [];
    for (var chartId in this.charts) {
        var $li = jQuery('<li/>', {
            'id': 'nav-' + chartId,
            'html': pick(KNOT.chartOptions[chartId].chart.relatedTitle, KNOT.chartOptions[chartId].title.text, chartId)
        }).click(KNOT.tabClickHandler);
        items.push($li);
    }
    if (items.length > 1) {
        for (var i = 0, iLength = items.length; i < iLength; i++) {
            $tabs.append(items[i]);
        }
        this._activateTabs(items);
        this._createInfoToggleButton();
        return;
    } else {
        // no tabs if only one chart is present
        return;
    }
};
KNOT.tabClickHandler = function () {
    var $this = jQuery(this),
        $tabs = jQuery('#benchmark-tabs');
    if ($this.hasClass('active')) {
        return;
    } else {
        var $activeTab = $tabs.children('li.active'),
            id = $this.attr('id').replace('nav-', '');
        if ($activeTab.length) {
            $activeTab.removeClass('active');
            KNOT.charts[$activeTab.attr('id').replace('nav-', '')].hide();
        }
        KNOT.charts[id].show();
        KNOT.setTabHash(id);
        $this.addClass('active');
    }
};
KNOT._activateTabs = function (tabs) {
    jQuery('.span12 > .section').hide();
    if (window.location.hash) {
        var hash = window.location.hash.substring(1);
        if (hash.indexOf('tab-') === 0) {
            var id = hash.replace('tab-', 'nav-');
            jQuery('#' + id).click();
            return;
        }
    }
    tabs[0].click();
    return;
};
KNOT._createInfoToggleButton = function () {
    jQuery('#benchmark-tabs').append(jQuery('<button/>', {
        id: 'benchmark-info-toggle-button',
        html: 'Hide/show benchmark information'
    }).click(function () {
        for (var chartId in KNOT.charts) {
            KNOT.charts[chartId].toggleInfo();
        }
    }));
};
KNOT.handleSeriesVisibility = function (serie) {
    if (serie.name) {
        var index = KNOT.hiddenSeriesNames.indexOf(serie.name);
        if (index >= 0) {
            KNOT.hiddenSeriesNames.splice(index, 1);
        } else {
            KNOT.hiddenSeriesNames.push(serie.name);
        }
    }
};
KNOT.hchNumFormat = function (value, decimals) {
    decimals = pick(decimals, 0);
    var decimalPoint = this.langPreferences.decimalPoint,
        thousandsSep = this.langPreferences.thousandsSep;
    return Highcharts.numberFormat(value, decimals, decimalPoint, thousandsSep);
};
Highcharts.setOptions({
    lang: KNOT.langPreferences
});
/**
 * Return the first value that is defined. Like MooTools' $.pick.
 */
function pick() {
    var args = arguments,
        i,
        arg,
        length = args.length;
    for (i = 0; i < length; i++) {
        arg = args[i];
        if (typeof arg !== 'undefined' && arg !== null) {
            return arg;
        }
    }
}
function deepcopy(object) {
    return jQuery.extend(true, {}, object);
}
