Benchmark
#########

:slug: benchmark
:lang: en
:menuorder: -1

.. raw:: html
   
   <h2>
      <a href="../../benchmark-100G/">100GbE</a> / <b>40GbE</b>
   </h2>

.. raw:: html
   :file: ../../benchmark/charts_loader.html

The primary objective of this benchmarking is to examine the performance limits
of authoritative name server implementations in various deployments.
All the scripts, data, and configurations can be found at `dns-benchmarking`_.

Results from measurement on previous hardware are still available at
`old benchmark`_.

The charts mentioned below currently represent measurement of the nameservers which
we think we are able to configure for optimal performance. We try to measure other
well-known nameserver implementations too, but we haven't found a reasonable setup
yet. Also, this is a very time-consuming process, therefore if interested, consider
a sponsorship to support further research and development.

*Disclaimer:* As many things can affect the measurement (hardware, operating system,
configuration, zone data, human mistakes, etc.), the results provided here are
informative only.

.. raw:: html

   <ul id="benchmark-tabs" class="nav-pills tabs"></ul>

Response Rate Benchmark
=======================

The testing infrastructure consists of two physical servers directly interconnected
via 40GbE. One server replays prepared DNS queries at various speeds to the
second server, which is running the testing name server software. The corresponding
responses are monitored on the querying server. The tool that is used for sending
queries is `kxdpgun`_.

The target hardware is AMD EPYC 7702P, Intel XL710 40GbE, 8x8 GiB RAM in
a Dell PowerEdge R6515 server.

Notes & Remarks
---------------

* SMT is disabled, the number of active CPU cores is 64 (UDP) or 16 (TCP)
* The number of network card channels is 64 (UDP) or 16 (TCP)
* The number of nameserver threads/processes is 64 (UDP) or 16 (TCP)
* Explicit *CFLAGS="-O2 -g -DNDEBUG"*
* Enabled *SO_REUSEPORT*, socket affinity (Knot DNS), and minimal responses
* Each server thread/process is bound to a CPU core (Knot DNS, NSD)
* Every query over TCP establishes a new connection
* Source IP addresses for TCP queries are from subnet /8 (IPv4) or /64 (IPv6)
* The measurement of each query rate takes 15 seconds (UDP) or 60 seconds (TCP)
* Deactivated connection tracking

.. _dns-benchmarking: https://gitlab.nic.cz/knot/dns-benchmarking
.. _100GbE Benchmark: ../../benchmark-100G/
.. _old benchmark: ../../benchmark-old/
.. _kxdpgun: https://www.knot-dns.cz/docs/latest/html/man_kxdpgun.html
