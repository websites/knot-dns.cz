Support
#######

:slug: support
:lang: en
:menu: Support
:menuorder: 5

The Knot DNS project is primarily sponsored by `CZ.NIC`_. In order to keep
sustainable development and increasing project quality, we offer several
professional support programmes. This form of commercial support allows
additional project sponsorship but also to get guaranteed user support, which
usually is necessary if the Knot DNS software is deployed in critical environment.

If you are interested in the support programmes, please contact us via
`knot-dns@labs.nic.cz`_.

Also we offer community user support, which is for free on a best-effort basis.
Users can choose a suitable support channel:

* Mailing list `knot-dns-users@lists.nic.cz`_
* Chat `Gitter`_
* `Issue tracker`_

.. _CZ.NIC: https://www.nic.cz
.. _knot-dns@labs.nic.cz: mailto:knot-dns@labs.nic.cz
.. _knot-dns-users@lists.nic.cz: https://lists.nic.cz/postorius/lists/knot-dns-users.lists.nic.cz
.. _Gitter: https://gitter.im/CZ-NIC/knot
.. _Issue tracker: https://gitlab.nic.cz/knot/knot-dns/issues
