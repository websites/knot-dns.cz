Development
###########

:slug: development
:lang: en
:menu: Development
:menuorder: 3

CZ.NIC Gitlab
=============

https://gitlab.nic.cz/knot/knot-dns.git

* `Issues or bug reports`_
* `Project Wiki`_
* Forking is enabled on demand – send us an e-mail with your username to `knot-dns@labs.nic.cz`_

GitHub – Mirrored repository
============================

https://github.com/CZ-NIC/knot.git

* Pull requests accepted

.. _Issues or bug reports: https://gitlab.nic.cz/knot/knot-dns/issues
.. _Project Wiki: https://gitlab.nic.cz/knot/knot-dns/-/wikis/home
.. _knot-dns@labs.nic.cz: mailto:knot-dns@labs.nic.cz
