Documentation
#############

:slug: documentation
:lang: en
:menu: Documentation
:menuorder: 2

Current Stable Branch
---------------------

* `Knot DNS 3.4 (HTML) <https://www.knot-dns.cz/docs/3.4/html/>`_
* `Knot DNS 3.4 (HTML, single page) <https://www.knot-dns.cz/docs/3.4/singlehtml/>`_
* `Knot DNS 3.4 (PDF) <https://www.knot-dns.cz/docs/3.4/latex/KnotDNS.pdf>`_
* `Knot DNS 3.4 (EPUB) <https://www.knot-dns.cz/docs/3.4/epub/KnotDNS.epub>`_

Previous Stable Branch
----------------------

* `Knot DNS 3.3 (HTML) <https://www.knot-dns.cz/docs/3.3/html/>`_
* `Knot DNS 3.3 (HTML, single page) <https://www.knot-dns.cz/docs/3.3/singlehtml/>`_
* `Knot DNS 3.3 (PDF) <https://www.knot-dns.cz/docs/3.3/latex/KnotDNS.pdf>`_
* `Knot DNS 3.3 (EPUB) <https://www.knot-dns.cz/docs/3.3/epub/KnotDNS.epub>`_

Development Branch
------------------

* `Knot DNS master (HTML) <https://knot.pages.nic.cz/knot-dns/master/html/>`_
* `Knot DNS master (HTML, single page) <https://knot.pages.nic.cz/knot-dns/master/singlehtml/>`_

Historical Branches
-------------------

* `Knot DNS 3.2 <https://www.knot-dns.cz/docs/3.2/singlehtml/>`_
* `Knot DNS 3.1 <https://www.knot-dns.cz/docs/3.1/singlehtml/>`_
* `Knot DNS 3.0 <https://www.knot-dns.cz/docs/3.0/singlehtml/>`_
* `Knot DNS 2.9 <https://www.knot-dns.cz/docs/2.9/singlehtml/>`_
* `Knot DNS 2.8 <https://www.knot-dns.cz/docs/2.8/singlehtml/>`_
* `Knot DNS 2.7 <https://www.knot-dns.cz/docs/2.7/singlehtml/>`_
* `Knot DNS 2.6 <https://www.knot-dns.cz/docs/2.6/singlehtml/>`_
* `Knot DNS 2.5 <https://www.knot-dns.cz/docs/2.5/singlehtml/>`_

