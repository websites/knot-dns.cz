Download
########

:slug: download
:lang: en
:menu: Download
:menuorder: 1

The Linux binary packages available here for download are maintained by the
Knot DNS development team. These packages may differ from the packages available in
the distributions. In addition to most of Linux distributions, native packages
are available also in BSD's and macOS. Every release version is a tag, a snapshot
of a respective branch. The API and ABI of the libraries don't change (except
for small compatible changes if necessary) during each branch life cycle.

Current Stable Branch
=====================

This `branch <curr_branch_>`_ (see the `changelog <curr_chgs_>`_)
has newer features, enhancements, and receives all
bug fixes. It's more modern, but less time-proven than the Previous Stable Branch.

.. _curr_branch: https://gitlab.nic.cz/knot/knot-dns/-/tree/3.4

.. list-table::
   :widths: 30 35 45 35 35 30 40
   :header-rows: 1

   * - Version
     - Release date
     - Source code
     - Debian packages
     - Ubuntu packages
     - RPM packages
     - `Docker Hub <curr_docker_>`_
   * - `Knot DNS 3.4.4 <curr_news_>`_
     - January 22, 2025
     - `Tarball <curr_tar_>`_, `asc <curr_asc_>`_, `sha256 <curr_sha_>`_, `Git <curr_git_>`_
     - `CZ.NIC <pkg_readme_>`_ `(old) <curr_deb_readme_>`_
     - `CZ.NIC <pkg_readme_>`_ `(old) <curr_ubu_readme_>`_
     - `Copr <curr_rpm_readme_>`_
     - ``cznic/knot:3.4``

.. _curr_chgs: https://gitlab.nic.cz/knot/knot-dns/-/raw/3.4/NEWS
.. _curr_news: https://gitlab.nic.cz/knot/knot-dns/-/releases/v3.4.4
.. _curr_tar:  https://secure.nic.cz/files/knot-dns/knot-3.4.4.tar.xz
.. _curr_asc:  https://secure.nic.cz/files/knot-dns/knot-3.4.4.tar.xz.asc
.. _curr_sha:  https://secure.nic.cz/files/knot-dns/knot-3.4.4.tar.xz.sha256
.. _curr_git:  https://gitlab.nic.cz/knot/knot-dns/-/tree/v3.4.4

.. _curr_deb_readme: https://deb.knot-dns.cz/knot-latest/README.txt
.. _curr_ubu_readme: https://launchpad.net/~cz.nic-labs/+archive/ubuntu/knot-dns-latest
.. _curr_rpm_readme: https://copr.fedorainfracloud.org/coprs/g/cznic/knot-dns-latest/
.. _curr_docker:     https://hub.docker.com/r/cznic/knot

Previous Stable Branch
======================

This `branch <prev_branch_>`_ (see the `changelog <prev_chgs_>`_)
is what used to be Current Stable Branch until it
had been replaced by a new Current Stable Branch.  Since then, it doesn't
receive new features or enhancements anymore, but it receives all important fixes.
It lacks the latest features, but it's more time-proven.

.. _prev_branch: https://gitlab.nic.cz/knot/knot-dns/-/tree/3.3

.. list-table::
   :widths: 30 35 45 35 35 30 40
   :header-rows: 1

   * - Version
     - Release date
     - Source code
     - Debian packages
     - Ubuntu packages
     - RPM packages
     - `Docker Hub <prev_docker_>`_
   * - `Knot DNS 3.3.10 <prev_news_>`_
     - December 12, 2024
     - `Tarball <prev_tar_>`_, `asc <prev_asc_>`_, `sha256 <prev_sha_>`_, `Git <prev_git_>`_
     - `CZ.NIC <pkg_readme_>`_ `(old) <prev_deb_readme_>`_
     - `CZ.NIC <pkg_readme_>`_ `(old) <prev_ubu_readme_>`_
     - `Copr <prev_rpm_readme_>`_
     - ``cznic/knot:3.3``

.. _prev_chgs: https://gitlab.nic.cz/knot/knot-dns/-/raw/3.3/NEWS
.. _prev_news: https://gitlab.nic.cz/knot/knot-dns/-/releases/v3.3.10
.. _prev_tar:  https://secure.nic.cz/files/knot-dns/knot-3.3.10.tar.xz
.. _prev_asc:  https://secure.nic.cz/files/knot-dns/knot-3.3.10.tar.xz.asc
.. _prev_sha:  https://secure.nic.cz/files/knot-dns/knot-3.3.10.tar.xz.sha256
.. _prev_git:  https://gitlab.nic.cz/knot/knot-dns/-/tree/v3.3.10

.. _prev_deb_readme: https://deb.knot-dns.cz/knot/README.txt
.. _prev_ubu_readme: https://launchpad.net/~cz.nic-labs/+archive/ubuntu/knot-dns
.. _prev_rpm_readme: https://copr.fedorainfracloud.org/coprs/g/cznic/knot-dns/
.. _prev_docker:     https://hub.docker.com/r/cznic/knot

.. _pkg_readme: https://pkg.labs.nic.cz/doc/?project=knot-dns
.. _pkg_repo:   https://pkg.labs.nic.cz/knot-dns/pool/main/k/knot/

Tarball signing GPG key
=======================

.. code-block:: none

    pub   rsa4096/0x10BB7AF6FEBBD6AB 2017-07-10 [SC] [expires: 2026-11-11]
          Key fingerprint = 742F A4E9 5829 B6C5 EAC6  B857 10BB 7AF6 FEBB D6AB
    uid   Daniel Salzman <daniel.salzman@nic.cz>

Miscellaneous
=============

* `All historical releases <https://secure.nic.cz/files/knot-dns/?C=N;O=D>`_
* `Libknot Python wrapper <https://pypi.org/project/libknot>`_
* `Prometheus exporter <https://pypi.org/project/knot-exporter>`_
* `REST API <https://gitlab.nic.cz/knot/knot-dns-rest>`_
