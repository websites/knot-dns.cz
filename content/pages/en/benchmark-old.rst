Old Benchmark
#############

:slug: benchmark-old
:lang: en
:menuorder: -1

.. raw:: html
   :file: ../../benchmark-old/charts_loader.html

*Disclaimer:* As many things can affect the measurement (hardware, operating system,
configuration, zone data, human mistakes, etc.), the results provided here are
informative only.

News
====

* December 10, 2019: `Epic DNS Benchmarking <https://en.blog.nic.cz/2019/12/10/epic-dns-benchmarking>`_

.. raw:: html

   <ul id="benchmark-tabs" class="nav-pills tabs"></ul>

Response Rate Benchmark
=======================

The test is based on two physical servers directly connected via 10GbE. One server
replays proper PCAP file at various speeds to the second server, which is running
the testing name-server software. Outgoing responses are monitored on the network
stack to eliminated various network issues.

Target hardware is Intel Xeon E5-2630V3 (HT enabled), Intel X710 10GbE (FW 6.80), 64 GiB RAM.

Utilized open-source tools are `tcpreplay`_ and `netmap`_.

Notes & Remarks
---------------

* Explicit *CFLAGS="-O2 -g -DNDEBUG"*
* Enabled *SO_REUSEPORT* (*SO_REUSEPORT_LB* on FreeBSD) and minimal responses
* Disabled RRSet rotation
* The number of threads/processes is the same as the number of CPU cores (16)
* The number of network card IO queues is 16 on Linux and 8 on FreeBSD
* libuv installed from `ISC's PPA`_

.. _dns-benchmarking: https://gitlab.nic.cz/knot/dns-benchmarking
.. _tcpreplay: http://tcpreplay.appneta.com
.. _netmap: https://github.com/luigirizzo/netmap
.. _ISC's PPA: https://launchpad.net/~isc/+archive/ubuntu/bind
