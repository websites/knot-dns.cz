Contact
#######

:slug: contact
:lang: en
:menu: Contact
:menuorder: 6

Email
=====

* `knot-dns@labs.nic.cz`_

.. _knot-dns@labs.nic.cz: mailto:knot-dns@labs.nic.cz

GPG key for secure communication
--------------------------------

.. code-block:: none

    pub   rsa4096/0x10BB7AF6FEBBD6AB 2017-07-10 [SC] [expires: 2026-11-11]
          Key fingerprint = 742F A4E9 5829 B6C5 EAC6  B857 10BB 7AF6 FEBB D6AB
    uid   Daniel Salzman <daniel.salzman@nic.cz>
    sub   rsa4096/0x5B82A3586624FF7F 2017-07-10 [E] [expires: 2026-11-11]

Address
=======

* CZ.NIC, z. s. p. o.

  Milesovska 1136/5

  130 00 Praha 3

  Czech Republic
