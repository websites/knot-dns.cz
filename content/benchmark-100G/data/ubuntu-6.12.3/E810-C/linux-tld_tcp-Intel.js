var chart = {
    'type': 'response-rate',
    'slug': 'linux-tld-Intel-1737034798',
    'date': '2025-01-16',
    'note': '<h1>TLD</h1>  <ul> <li>Zones: 1</li> <li>DNSSEC: no</li> <li>Records: 4479959 total, 1400337 delegations</li> <li>Queries: random QNAME, 0% DO</li> <li>Replies: 100% NOERROR</li> <li>Protocol: IPv4 TCP</li> </ul> ',
    'properties': {
        'os': 'Linux 6.12.3',
        'deployment': '<b>TCP</b> TLD',
        'adapter': 'Intel E810-C'
    },
    'series': [
{ name: 'Knot DNS 3.4.3 XDP', custom: { ip_size: 46, avg_resp_size: 108}, data: [  [9994,100.00], [200010,100.00], [400010,100.00], [600010,100.00], [800010,100.00], [1000010,100.00], [1200010,100.00], [1400010,100.00], [1600010,100.00], [1800010,100.00], [2000010,99.99], [2200010,99.99], [2400010,99.99], [2600010,99.99], [2800010,99.99], [3000010,99.99], [3200010,99.99], [3400010,99.99], [3600010,99.99], [3800010,99.99], [4000010,99.99], [4200010,99.99], [4400010,99.99], [4600008,99.99], [4800009,99.99], [5000008,99.98], ] }
]};

KNOT.rawChartData.push(chart);
