var chart = {
    'type': 'response-rate',
    'slug': 'linux-tld_ipv6-Intel-1737049064',
    'date': '2025-01-16',
    'note': '<h1>TLD IPv6</h1>  <ul> <li>Zones: 1</li> <li>DNSSEC: no</li> <li>Records: 4479959 total, 1400337 delegations</li> <li>Queries: random QNAME, 0% DO</li> <li>Replies: 100% NOERROR</li> <li>Protocol: IPv6 TCP</li> </ul> ',
    'properties': {
        'os': 'Linux 6.12.3',
        'deployment': '<b>TCP</b> TLD IPv6',
        'adapter': 'Intel E810-C'
    },
    'series': [
{ name: 'Knot DNS 3.4.3 XDP', custom: { ip_size: 66, avg_resp_size: 108}, data: [  [9994,100.00], [200010,100.00], [400010,100.00], [600010,100.00], [800010,100.00], [1000010,100.00], [1200010,100.00], [1400010,100.00], [1600010,100.00], [1800010,100.00], [2000010,95.82], [2200010,93.91], [2400010,91.28], [2600010,88.36], [2800010,84.25], [3000010,84.11], [3200010,80.88], [3400010,82.03], [3600010,78.25], [3800010,75.58], [4000010,72.48], [4200010,65.99], [4400010,68.03], [4600010,64.99], [4800010,64.00], [5000007,60.19], ] } 
]};

KNOT.rawChartData.push(chart);
