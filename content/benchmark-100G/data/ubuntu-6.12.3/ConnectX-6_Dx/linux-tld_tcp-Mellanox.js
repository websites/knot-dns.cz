var chart = {
    'type': 'response-rate',
    'slug': 'linux-tld-Mellanox-1736496097',
    'date': '2025-01-10',
    'note': '<h1>TLD</h1>  <ul> <li>Zones: 1</li> <li>DNSSEC: no</li> <li>Records: 4479959 total, 1400337 delegations</li> <li>Queries: random QNAME, 0% DO</li> <li>Replies: 100% NOERROR</li> <li>Protocol: IPv4 TCP</li> </ul> ',
    'properties': {
        'os': 'Linux 6.12.3',
        'deployment': '<b>TCP</b> TLD',
        'adapter': 'Mellanox ConnectX-6 Dx'
    },
    'series': [
//{ name: 'Knot DNS 3.4.3', custom: { ip_size: 46, avg_resp_size: 108}, data: [  [9994,98.92], [200010,99.56], [400010,82.04], [600010,40.67], [800010,29.39], [1000010,22.53], [1200010,17.90], [1400010,14.55], [1600010,11.86], [1800010,10.02], [2000010,8.40], [2200010,7.29], [2400010,6.32], [2600010,5.47], [2800010,5.04], [3000010,4.58], [3200010,4.16], [3400010,3.84], [3600010,3.57], [3800009,3.32], [4000010,3.08], [4200010,2.89], [4400010,2.64], [4600010,2.52], [4800010,2.38], [5000010,2.28], ] }
// , 
{ name: 'Knot DNS 3.4.3 XDP', custom: { ip_size: 46, avg_resp_size: 108}, data: [  [9994,100.00], [200010,100.00], [400010,100.00], [600010,100.00], [800010,100.00], [1000010,100.00], [1200010,100.00], [1400010,100.00], [1600010,100.00], [1800010,100.00], [2000010,100.00], [2200010,100.00], [2400010,100.00], [2600010,100.00], [2800010,100.00], [3000010,99.99], [3200010,99.99], [3400010,99.99], [3600010,99.99], [3800010,99.99], [4000010,99.99], [4200010,99.99], [4400005,99.95], [4600006,99.70], [4800005,99.21], [5000006,97.48], ] } 
]};

KNOT.rawChartData.push(chart);
