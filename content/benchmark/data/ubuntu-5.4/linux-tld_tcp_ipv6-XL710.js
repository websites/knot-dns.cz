var chart = {
    'type': 'response-rate',
    'slug': 'linux-tld_tcp_ipv6-XL710-1627645561',
    'date': '2021-07-30',
    'note': '<h1>TCP TLD IPv6</h1>  <ul> <li>Zones: 1</li> <li>DNSSEC: no</li> <li>Records: 5182707 total, 1479662 delegations</li> <li>Queries: random QNAME, 0% DO</li> <li>Replies: 100% NOERROR</li> <li>Protocol: IPv6</li> </ul> ',
    'properties': {
        'os': 'Linux 5.4.0',
        'deployment': '<b>TCP</b> TLD IPv6',
        'adapter': 'Intel XL710'
    },
    'series': [
{ name: 'Knot DNS 3.1.0', custom: { ip_size: 66, avg_resp_size: 110 }, data: [  [10024,97.30], [100016,94.23], [200032,97.25], [300020,93.03], [400036,75.10], [500024,59.83], [600040,47.97], [700028,40.10], [800016,37.13], [900030,32.89], [1000020,30.75], [1100032,26.81], [1200024,24.82], [1300030,21.75], [1400028,19.81], [1500016,18.37], [1600010,16.63], [1700020,15.21], [1800016,13.96], [1900024,12.83], [2000012,11.67], [2100028,10.82], [2200016,10.05], [2300004,8.85], [2400018,8.18], [2500010,7.64], [2600024,6.99], [2700012,6.29], [2800026,5.72], [2900014,5.16], [3000006,4.54], [3100018,4.17], [3200012,3.71], [3300020,3.44], [3400018,3.09], [3500022,2.74], ] }
 , 
{ name: 'Knot DNS 3.1.0 XDP', custom: { ip_size: 66, avg_resp_size: 110 }, data: [  [10024,100.00], [100016,100.00], [200032,100.00], [300020,100.00], [400036,100.00], [500024,100.00], [600038,100.00], [700028,100.00], [800016,99.99], [900020,100.00], [1000020,99.98], [1100020,99.65], [1200022,99.14], [1300020,99.22], [1400022,98.91], [1500016,98.24], [1600002,95.62], [1700010,94.41], [1800012,90.49], [1900004,86.08], [2000014,81.92], [2100016,78.21], [2200004,74.81], [2300016,73.05], [2400008,69.00], [2500004,64.65], [2600010,62.89], [2700016,60.43], [2800012,57.57], [2900004,55.72], [3000000,52.88], [3100012,51.69], [3199996,49.11], [3300010,47.42], [3400004,45.02], [3499998,44.94], ] }
]};

KNOT.rawChartData.push(chart);
