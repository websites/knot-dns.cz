====================================
Pelican sources for Knot DNS website
====================================

Preparation
===========

Creating virtualenv
-------------------

This is not mandatory, but it's handy to create separate environments for such projects, so you don't have to install (possibly conflicting) dependencies system-wide.

There are good tutorials for virtualenv `available <https://iamzed.com/2009/05/07/a-primer-on-virtualenv/>`_ `online <http://docs.python-guide.org/en/latest/dev/virtualenvs/>`_, but essentially it's just this::

    >> virtualenv knot-website
    >> cd knot-website
    >> source bin/activate

You can use either python 2 or 3, the build system works with both (tested on 2.7.x and 3.4.x).
However, please test it with python 2.7 after making bigger changes in pelican config or template, as Jenkins uses this version by default.
Virtualenv can be told to use specific python binary::

    >> virtualenv -p `which python2.7` knot-website

Replace ``python2.7`` with actual binary name of course. This one works at least on Gentoo and Debian/Ubuntu.

Then just clone this repository, ``cd`` into it and you are good to go.

Installing dependencies
-----------------------

**This is what you need to do only after fetching the source for the first
time or after updates.**

After cloning the repository, you have to do the following do make it work::

    >> git submodule init
    >> git submodule update

This will fetch the ``pelican-plugins`` and ``product-template`` repos as git submodules.

Then you need to install requirements. You can do it by::

    >> pip install -r requirements.txt

at best inside a virtualenv specifically created for this application.

If you encounter something like ``fatal error: Python.h: No such file or directory``
during ``pip install`` and you're on Debian/Ubuntu, you need to install ``python-dev`` deb package.

Usage
=====

Building
--------

You can build the website by running::

    >> make

in the main directory.

Default ``make`` target uses URLs set in ``publishconf.py``.
For local development/testing, use::

    >> make build-dev

The output is in the ``output`` directory, but it is
better to view it by doing::

    >> cd output
    >> python -m SimpleHTTPServer        # for python 2
    >> python -m http.server             # for python 3

and opening http://localhost:8000 in your browser, because you will see all
the styling etc. as well (not everything works as expected with ``file://``
paths in modern browsers).

You can specify another port if 8000 is used by something else::

    >> python -m http.server 1234

Publishing
----------

Just commit to ``master`` branch and push to Gitlab. The website is automatically
built by a `Jenkins job <https://jenkins.labs.nic.cz/job/knot-dns.cz/>`_.

Usually, it takes about a minute for Jenkins to notice new commit(s), fetch everything,
and finish the build. You can view build progress and result via the Jenkins'
web interface.

Build output is then picked up by a cron job and published to webserver.

Adding news
-----------

Create a new ``.md`` or ``.rst`` file in ``content/news/<language>/``.

(`Pelican docs here
<http://docs.getpelican.com/en/latest/content.html#file-metadata>`_).

Adding a page
-------------

`Pelican docs <http://docs.getpelican.com/en/latest/content.html#pages>_`.

Create a new ``.md`` or ``.rst`` file in ``content/pages/<language>/``. Syntax is similar to news ::

New page will be available at ``knot-dns.cz/<lang>/<slug>.html``, so **page slugs
need to be unique** for given language.

Adding a footer link (Ostatní weby / Other sites)
-------------------------------------------------

Edit ``FOOTER_LINKS`` in ``pelicanconf.py``.

Changing social network URLs
----------------------------

Edit ``SOCIAL`` in ``pelicanconf.py``.
