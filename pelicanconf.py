#!/usr/bin/env python
# -*- coding: utf-8 -*- #

from __future__ import unicode_literals
import re
import pyphen

SITENAME = "Knot DNS"
SITESUBTITLE = "High-performance authoritative DNS server"
SITE_LOGO = "images/logo.svg"
SITEURL = "https://www.knot-dns.cz"
NEWS_TITLE = "News"
DOWNLOAD_BUTTON = "Download"
DOWNLOAD_URL = "/download/"

# Settings for GitLab pages
PATH = 'content'
OUTPUT_PATH = 'public'

# PARTNERS = (
#     ("HEARST", "hearst.svg"),
#     ("RIPE NCC", "ripe.svg"),
#     ("Microsoft", "microsoft.svg"),
#     ("GitHub", "github.svg"),
#     ("ICANN", "icann.svg")
# )

#VIDEO_ID = "3PIE_bi1YxE"

TIMEZONE = "Europe/Prague"

DEFAULT_LANG = "en"
LOCALE = "en_US.UTF-8"
DEFAULT_DATE_FORMAT = "%A, %B %-d, %Y"

FOOTER_LINKS = (
    ("Labs", "https://labs.nic.cz/"),
    ("FRED", "http://fred.nic.cz/"),
    ("BIRD", "http://bird.network.cz/"),
    ("Knot Resolver", "https://knot-resolver.cz/"),
    ("Turris Omnia", "https://omnia.turris.cz/"),
    ("CSIRT", "https://www.csirt.cz/"),
    ("Turris", "https://www.turris.cz/"),
    ("Web scanner", "https://www.skenerwebu.cz/")
)

FEATURES = (
    ("Open source", "open_source.svg",
     "Knot DNS is open-source. It is completely free to download and use. The "
     "source code is available under GPL 3+ license. Our development process "
     "is transparent and driven by the needs of community and donating users. "
     "The target platforms are Linux, BSD's, macOS, and other POSIX operating "
     "systems."),
    ("Feature-packed", "feature_packed.svg",
     "Knot DNS provides essential DNS features including incremental zone "
     "transfers (IXFR), dynamic updates (DDNS) and response rate limiting "
     "(RRL). More advanced features include automatic DNSSEC signing, dynamic "
     "A/AAAA/PTR records synthesis, or rapid on-the-fly reconfiguration."),
    ("High performance", "high_performance.svg",
     "The server is suitable for any use case. With its high performace, "
     "unmatched DNSSEC implementation, and other features it works "
     "exceptionally well as a root or a TLD name server. "
     "It's capable of non-stop operation. The responding code is completely "
     "lock-free and therefore the server achieves very high response rate."),
    ("Secure and stable", "secure_and_stable.svg",
     "In addition to performace, security and stability are the other key "
     "goals of the design. The code is being constantly checked by an "
     "extensive testing suite to attain stability, assure interoperability "
     "with other DNS implementations, avoid performance regressions, and "
     "circumvent possible security-related problems.")
)

BANNER = {
    "link": "https://www.knot-resolver.cz/",
    "logo": "knot_resolver.svg",
    "title": "Knot Resolver",
    "text": """The Knot Resolver is a caching full resolver implementation written in C and LuaJIT,
            including both a resolver library and a daemon. Modular architecture of the library
            keeps the core tiny and efficient, and provides a state-machine-like API."""
}

NON_BREAKABLE_WORDS = [
    "Knot",
    "(RRL).",
    "(IXFR),",
    "DNS",
    "DNSSEC",
    "LuaJIT,",
    "TLD",
    "A/AAAA/PTR"
]


def hyphens(text):
    """
    Jinja2 filter.
    Adds &shy; marks to suggest word breaks.
    Usage: {{ page.foo | hyphens }}.
    """
    dic = pyphen.Pyphen(lang=DEFAULT_LANG)
    wordlist = text.split()
    hyphenated = []
    for word in wordlist:
        if word in NON_BREAKABLE_WORDS:
            hyphenated.append(word)
        else:
            hyphenated.append(dic.inserted(word, "&shy;"))
    return " ".join(hyphenated)


def dont_break(text):
    """
    Jinja2 filter.
    Adds a non-breakable space after short prepositions.
    Usage: {{ page.foo | dont_break }}
    """
    shortwords = r"([aA]|[Aa]nd?|[Pr]ro|[Th]he|[Oo]f|[Tt]o|[Ii]t|[Ii]s|[Bb]y)\s"
    return re.sub(shortwords, r"\1&nbsp;", text)

I18N_UNTRANSLATED_PAGES = "remove"
I18N_UNTRANSLATED_ARTICLES = "remove"

DEFAULT_PAGINATION = 1

THEME = "product-template"

PIWIK_URL = "piwik.nic.cz"
PIWIK_SITE_ID = 3

# static paths will be copied under the same name
STATIC_PATHS = ["images", "docs", "js", "css", "benchmark", "benchmark-old", "benchmark-100G"]
PAGE_EXCLUDES = ["docs"]
ARTICLE_EXCLUDES = ["docs"]

def lookup_lang_name(lang_code):
    """
    Jinja2 filter.
    Returns full name of site language.
    """
    return languages_lookup[lang_code]


PLUGIN_PATHS = ["./pelican-plugins"]
ASSET_SOURCE_PATHS = ["static"]

PLUGINS = [
    "minify",
    "assets",
    "i18n_subsites"
]

JINJA_FILTERS = {
    "lookup_lang_name": lookup_lang_name,
    "dont_break": dont_break,
    "hyphens": hyphens
}

JINJA_ENVIRONMENT = {
    "extensions": ["jinja2.ext.i18n"]
}

FEED_ALL_RSS = "feeds/knot.xml"

SOCIAL = (("Twitter", "https://twitter.com/KnotDNS"),
          )

ARTICLE_URL = "{date:%Y}-{date:%m}-{date:%d}-{slug}.html"
ARTICLE_SAVE_AS = ARTICLE_URL

CATEGORY_SAVE_AS = ""
TAG_SAVE_AS = ""
AUTHOR_SAVE_AS = ""
ARCHIVES_SAVE_AS = ""
AUTHORS_SAVE_AS = ""
CATEGORIES_SAVE_AS = ""
TAGS_SAVE_AS = ""
PAGE_SAVE_AS = "{slug}/index.html"
PAGE_URL = "{slug}/"

RELATIVE_URLS = True

SUMMARY_MAX_LENGTH = 30
IGNORE_FILES = ["charts_loader.html"]

MINIFY = {
    "remove_comments": True,
    "remove_all_empty_space": False,
    "remove_optional_attribute_quotes": False
}
