#!/usr/bin/env python
# -*- coding: utf-8 -*- #

from __future__ import unicode_literals
import sys

sys.path.append('.')
from pelicanconf import *

SITEURL = "https://www.knot-dns.cz"
